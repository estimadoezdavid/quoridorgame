package primeraEntrega;

import java.util.Scanner;

public class Main {

	public static void main(String args[]) {
		Scanner entradaEscaner = new Scanner(System.in);

		System.out.println("Bienvenidos al juego Quoridor");
		System.out.println("Ingrese la cantidad de jugadores. Recuerde que deben ser 2 o 4:");
		int cantidadJugadores = 0;
		cantidadJugadores = Integer.parseInt(entradaEscaner.nextLine());
		if (!(cantidadJugadores == 2 || cantidadJugadores == 4)) {
			System.out.println("Valor no v�lido, por favor reinicie el juego...");
			System.exit(0);
		}

		if (cantidadJugadores == 4) {
			System.out.println("_____________________________________");
			System.out.println("|___|___|___|___|_O_|___|___|___|___|");
			System.out.println("|___|___|___|___|___|___|___|___|___|");
			System.out.println("|___|___|___|___|___|___|___|___|___|");
			System.out.println("|___|___|___|___|___|___|___|___|___|");
			System.out.println("|_O_|___|___|___|___|___|___|___|_O_|");
			System.out.println("|___|___|___|___|___|___|___|___|___|");
			System.out.println("|___|___|___|___|___|___|___|___|___|");
			System.out.println("|___|___|___|___|___|___|___|___|___|");
			System.out.println("|___|___|___|___|_O_|___|___|___|___|");
		} else {
			System.out.println("�C�mo desean tener la perspectiva del juego?");
			System.out.println("A. Vertical");
			System.out.println("B. Horizontal");
			String perspectivaJuego = "";
			perspectivaJuego = entradaEscaner.nextLine();
			if (perspectivaJuego.equalsIgnoreCase("a")) {
				System.out.println("_____________________________________");
				System.out.println("|___|___|___|___|_O_|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|_O_|___|___|___|___|");
			} else if (perspectivaJuego.equalsIgnoreCase("b")) {
				System.out.println("_____________________________________");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|_O_|___|___|___|___|___|___|___|_O_|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
				System.out.println("|___|___|___|___|___|___|___|___|___|");
			} else {
				System.out.println("Valor no v�lido, por favor reinicie el juego...");
				System.exit(0);
			}
		}
	}

}