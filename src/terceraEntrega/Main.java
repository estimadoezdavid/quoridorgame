package terceraEntrega;

import java.util.Scanner;

public class Main {

	public static void main(String args[]) {
		Scanner entradaEscaner = new Scanner(System.in);

		System.out.println("\nBienvenidos al juego Quoridor");
		int cantidadJugadores = 0;
		boolean estadoCantidadJugadores = false;
		while (!(cantidadJugadores == 2 || cantidadJugadores == 4)) {
			if (estadoCantidadJugadores) {
				System.out.println("\nCantidad de jugadores incorrecta...");
			}
			System.out.println("\nIngrese la cantidad de jugadores.");
			System.out.println("Recuerden que deben ser 2 o 4:");
			cantidadJugadores = Integer.parseInt(entradaEscaner.nextLine());
			estadoCantidadJugadores = true;
		}

		String[] nombresJugadores = new String[cantidadJugadores];
		for (int i = 0; i < cantidadJugadores; i++) {
			System.out.println("\nIngrese el nombre del jugador N�" + (i + 1));
			nombresJugadores[i] = entradaEscaner.nextLine();
		}

		String techosYPisos = "   +---+---+---+---+---+---+---+---+---+";
		String filaSinFichasYSinParedes = "|   |   |   |   |   |   |   |   |   |";
		String fila = "";
		String techosYPisosConPared = "";
		String casillaSinFicha = "|   |";
		String casillaConFichaUno = "| 1 |";
		String casillaConFichaDos = "| 2 |";
		String casillaConFichaTres = "| 3 |";
		String casillaConFichaCuatro = "| 4 |";
		String paredHorizontal = "+---+";
		String paredHorizontalParaFichaUno = "+:::+";
		String paredHorizontalParaFichaDos = "+===+";
		String paredVerticalParaFichaUno = "I";
		String paredVerticalParaFichaDos = "$";

		if (cantidadJugadores == 2) {
			String perspectivaJuego = "";
			boolean estadoPerspectivaJuego = false;
			while (!(perspectivaJuego.equalsIgnoreCase("a") || perspectivaJuego.equalsIgnoreCase("b"))) {
				if (estadoPerspectivaJuego) {
					System.out.println("\nOpci�n incorrecta...");
				}
				System.out.println("\n�C�mo desean tener la perspectiva del juego?");
				System.out.println("A. Vertical");
				System.out.println("B. Horizontal");
				System.out.println("Ingresar la opci�n en may�scula o min�scula...");
				perspectivaJuego = entradaEscaner.nextLine();
				estadoPerspectivaJuego = true;
			}
			System.out.println("");
			boolean[][] posicionesParedesHorizontales = new boolean[8][9];
			boolean[][] posicionesParedesVerticales = new boolean[9][8];
			if (perspectivaJuego.equalsIgnoreCase("a")) {

				boolean[][] posicionesFichas = new boolean[9][9];
				int filaActualJugadorUno = 0;
				int filaActualJugadorDos = 8;
				int columnaActualJugadorUno = 4;
				int columnaActualJugadorDos = 4;

				for (int i = 0; i < 9; i++) {
					for (int j = 0; j < 9; j++) {
						if ((i == filaActualJugadorUno && j == columnaActualJugadorUno)
								|| (i == filaActualJugadorDos && j == columnaActualJugadorDos)) {
							posicionesFichas[i][j] = true;
						}
					}
				}

				int numeroFicha = 1;
				System.out.println("     1   2   3   4   5   6   7   8   9     ");
				int numeroFilas = 1;
				int coordenadaY = 1;
				for (int i = 0; i < 19; i++) {
					if (i % 2 == 0) {
						if (coordenadaY != 10) {
							System.out.println(techosYPisos + " " + coordenadaY + " ");
						} else {
							System.out.println(techosYPisos + " " + coordenadaY);
						}
						coordenadaY++;
						if ((i + 1) == 19) {
							System.out.println("   1   2   3   4   5   6   7   8   9   10  ");
						}
					} else {
						if (i == 1 || i == 17) {
							System.out.println(
									" " + numeroFilas + " |   |   |   |   | " + numeroFicha + " |   |   |   |   |   ");
							numeroFicha++;
						} else {
							System.out.println(" " + numeroFilas + " " + filaSinFichasYSinParedes + "   ");
						}
						numeroFilas++;
					}
				}

				boolean juegoTerminado = false;
				int jugadorActual = 0;
				boolean moverAdelante = true;
				boolean moverAtras = true;
				boolean moverLadoIzquierdo = true;
				boolean moverLadoDerecho = true;
				int y = 0;
				int columnaUno = 0;
				int columnaDos = 0;
				int x = 0;
				int filaUno = 0;
				int filaDos = 0;

				while (!juegoTerminado) {
					System.out.print("\nPresione <Enter> para continuar...");
					entradaEscaner.nextLine();

					moverAdelante = true;
					moverAtras = true;
					moverLadoIzquierdo = true;
					moverLadoDerecho = true;

					for (int i = 0; i < 9; i++) {
						for (int j = 0; j < 9; j++) {
							if (jugadorActual == 0) {
								if (posicionesFichas[i][j]) {
									if (i == 0 && filaActualJugadorUno == i) {
										moverAtras = false;
									} else if (i == 8 && filaActualJugadorUno == i) {
										moverAdelante = false;
									}

									if (j == 0 && columnaActualJugadorUno == j) {
										moverLadoDerecho = false;
									} else if (j == 8 && columnaActualJugadorUno == j) {
										moverLadoIzquierdo = false;
									}
								}
							} else {
								if (posicionesFichas[i][j]) {
									if (i == 8 && filaActualJugadorDos == i) {
										moverAtras = false;
									} else if (i == 0 && filaActualJugadorDos == i) {
										moverAdelante = false;
									}

									if (j == 8 && columnaActualJugadorDos == j) {
										moverLadoDerecho = false;
									} else if (j == 0 && columnaActualJugadorDos == j) {
										moverLadoIzquierdo = false;
									}
								}
							}
						}
					}

					System.out.println("\nEs tu turno " + nombresJugadores[jugadorActual]);
					String tipoJugada = "";
					boolean estadoTipoJugada = false;
					while (!(tipoJugada.equalsIgnoreCase("a") || tipoJugada.equalsIgnoreCase("b"))) {
						if (estadoTipoJugada) {
							System.out.println("\nOpci�n incorrecta...");
						}
						System.out.println(
								"\n" + nombresJugadores[jugadorActual] + ", �qu� tipo de jugada quieres realizar?");
						System.out.println("A. Mover ficha");
						System.out.println("B. Situar pared");
						System.out.println("Ingresar la opci�n en may�scula o min�scula...");
						tipoJugada = entradaEscaner.nextLine();
						estadoTipoJugada = true;
					}

					System.out.println("");

					if (tipoJugada.equalsIgnoreCase("a")) {
						String movimiento = "";
						boolean estadoMovimiento = false;
						while (!(movimiento.equalsIgnoreCase("a") || movimiento.equalsIgnoreCase("b")
								|| movimiento.equalsIgnoreCase("c") || movimiento.equalsIgnoreCase("d"))) {
							if (estadoMovimiento) {
								System.out.println("\nOpci�n incorrecta...");
							}
							System.out
									.println(nombresJugadores[jugadorActual] + ", �para d�nde quieres mover tu ficha?");
							if (moverAdelante) {
								System.out.println("A. Adelante");
							}
							if (moverAtras) {
								System.out.println("B. Atr�s");
							}
							if (moverLadoIzquierdo) {
								System.out.println("C. Lado izquierdo");
							}
							if (moverLadoDerecho) {
								System.out.println("D. Lado derecho");
							}
							System.out.println("Ingresar la opci�n en may�scula o min�scula...");
							movimiento = entradaEscaner.nextLine();
							estadoMovimiento = true;
						}

						if (movimiento.equalsIgnoreCase("a")) {
							if (jugadorActual == 0) {
								filas: for (int i = 0; i < 9; i++) {
									for (int j = 0; j < 9; j++) {
										if (i == (filaActualJugadorUno + 1) && j == columnaActualJugadorUno) {
											if (posicionesFichas[i][j] && i != 8) {
												posicionesFichas[i + 1][j] = true;
												posicionesFichas[filaActualJugadorUno][columnaActualJugadorUno] = false;
												filaActualJugadorUno = i + 1;
											} else {
												posicionesFichas[i][j] = true;
												posicionesFichas[filaActualJugadorUno][columnaActualJugadorUno] = false;
												filaActualJugadorUno = i;
											}
											break filas;
										}
									}
								}
							} else {
								filas: for (int i = 0; i < 9; i++) {
									for (int j = 0; j < 9; j++) {
										if (i == (filaActualJugadorDos - 1) && j == columnaActualJugadorDos) {
											if (posicionesFichas[i][j] && i != 0) {
												posicionesFichas[i - 1][j] = true;
												posicionesFichas[filaActualJugadorDos][columnaActualJugadorDos] = false;
												filaActualJugadorDos = i - 1;
											} else {
												posicionesFichas[i][j] = true;
												posicionesFichas[filaActualJugadorDos][columnaActualJugadorDos] = false;
												filaActualJugadorDos = i;
											}
											break filas;
										}
									}
								}
							}
						} else if (movimiento.equalsIgnoreCase("b")) {
							if (jugadorActual == 0) {
								filas: for (int i = 0; i < 9; i++) {
									for (int j = 0; j < 9; j++) {
										if (i == (filaActualJugadorUno - 1) && j == columnaActualJugadorUno) {
											if (posicionesFichas[i][j] && i != 0) {
												posicionesFichas[i - 1][j] = true;
												posicionesFichas[filaActualJugadorUno][columnaActualJugadorUno] = false;
												filaActualJugadorUno = i - 1;
											} else {
												posicionesFichas[i][j] = true;
												posicionesFichas[filaActualJugadorUno][columnaActualJugadorUno] = false;
												filaActualJugadorUno = i;
											}
											break filas;
										}
									}
								}
							} else {
								filas: for (int i = 0; i < 9; i++) {
									for (int j = 0; j < 9; j++) {
										if (i == (filaActualJugadorDos + 1) && j == columnaActualJugadorDos) {
											if (posicionesFichas[i][j] && i != 8) {
												posicionesFichas[i + 1][j] = true;
												posicionesFichas[filaActualJugadorDos][columnaActualJugadorDos] = false;
												filaActualJugadorDos = i + 1;
											} else {
												posicionesFichas[i][j] = true;
												posicionesFichas[filaActualJugadorDos][columnaActualJugadorDos] = false;
												filaActualJugadorDos = i;
											}
											break filas;
										}
									}
								}
							}
						} else if (movimiento.equalsIgnoreCase("c")) {
							if (jugadorActual == 0) {
								filas: for (int i = 0; i < 9; i++) {
									for (int j = 0; j < 9; j++) {
										if (i == filaActualJugadorUno && j == (columnaActualJugadorUno + 1)) {
											if (posicionesFichas[i][j] && j != 8) {
												posicionesFichas[i][j + 1] = true;
												posicionesFichas[filaActualJugadorUno][columnaActualJugadorUno] = false;
												columnaActualJugadorUno = j + 1;
											} else {
												posicionesFichas[i][j] = true;
												posicionesFichas[filaActualJugadorUno][columnaActualJugadorUno] = false;
												columnaActualJugadorUno = j;
											}
											break filas;
										}
									}
								}
							} else {
								filas: for (int i = 0; i < 9; i++) {
									for (int j = 0; j < 9; j++) {
										if (i == filaActualJugadorDos && j == (columnaActualJugadorDos - 1)) {
											if (posicionesFichas[i][j] && j != 0) {
												posicionesFichas[i][j - 1] = true;
												posicionesFichas[filaActualJugadorDos][columnaActualJugadorDos] = false;
												columnaActualJugadorDos = j - 1;
											} else {
												posicionesFichas[i][j] = true;
												posicionesFichas[filaActualJugadorDos][columnaActualJugadorDos] = false;
												columnaActualJugadorDos = j;
											}
											break filas;
										}
									}
								}
							}
						} else if (movimiento.equalsIgnoreCase("d")) {
							if (jugadorActual == 0) {
								filas: for (int i = 0; i < 9; i++) {
									for (int j = 0; j < 9; j++) {
										if (i == filaActualJugadorUno && j == (columnaActualJugadorUno - 1)) {
											if (posicionesFichas[i][j] && j != 0) {
												posicionesFichas[i][j - 1] = true;
												posicionesFichas[filaActualJugadorUno][columnaActualJugadorUno] = false;
												columnaActualJugadorUno = j - 1;
											} else {
												posicionesFichas[i][j] = true;
												posicionesFichas[filaActualJugadorUno][columnaActualJugadorUno] = false;
												columnaActualJugadorUno = j;
											}
											break filas;
										}
									}
								}
							} else {
								filas: for (int i = 0; i < 9; i++) {
									for (int j = 0; j < 9; j++) {
										if (i == filaActualJugadorDos && j == (columnaActualJugadorDos + 1)) {
											if (posicionesFichas[i][j] && j != 8) {
												posicionesFichas[i][j + 1] = true;
												posicionesFichas[filaActualJugadorDos][columnaActualJugadorDos] = false;
												columnaActualJugadorDos = j + 1;
											} else {
												posicionesFichas[i][j] = true;
												posicionesFichas[filaActualJugadorDos][columnaActualJugadorDos] = false;
												columnaActualJugadorDos = j;
											}
											break filas;
										}
									}
								}
							}
						}
					} else if (tipoJugada.equalsIgnoreCase("b")) {
						String orientacionPared = "";
						boolean estadoOrientacionPared = false;
						while (!(orientacionPared.equalsIgnoreCase("a") || orientacionPared.equalsIgnoreCase("b"))) {
							if (estadoOrientacionPared) {
								System.out.println("\nOpci�n incorrecta...");
							}
							System.out.println("\n" + nombresJugadores[jugadorActual]
									+ ", �en qu� orientaci�n quieres situar la pared?");
							System.out.println("A. Horizontal");
							System.out.println("B. Vertical");
							System.out.println("Ingresar la opci�n en may�scula o min�scula...");
							orientacionPared = entradaEscaner.nextLine();
							estadoOrientacionPared = true;
						}

						System.out.println("");

						if (orientacionPared.equalsIgnoreCase("a")) {
							y = 0;
							boolean estadoY = false;
							while (!(y > 0 && y < 9)) {
								if (estadoY) {
									System.out.println("\nCoordenada incorrecta...");
								}
								System.out.println("\n" + nombresJugadores[jugadorActual]
										+ ", �en qu� coordenada del eje Y quiere situar la pared?");
								y = Integer.parseInt(entradaEscaner.nextLine());
								estadoY = true;
							}
							System.out.println("\n" + nombresJugadores[jugadorActual]
									+ ", �en qu� columnas quiere situar la pared?");
							columnaUno = 0;
							columnaDos = 0;
							boolean estadoColumna = false;
							while (columnaUno == columnaDos
									&& (!(columnaUno > 0 && columnaUno < 10) || !(columnaDos > 0 && columnaDos < 10))) {
								if (estadoColumna) {
									System.out.println("\nColumna incorrecta...");
								}
								if (!(columnaUno > 0 && columnaUno < 10)) {
									System.out.println("Ingrese el valor de la primera columna:");
									columnaUno = Integer.parseInt(entradaEscaner.nextLine());
								}
								if (!(columnaDos > 0 && columnaDos < 10)) {
									System.out.println("Ingrese el valor de la segunda columna:");
									columnaDos = Integer.parseInt(entradaEscaner.nextLine());
								}
								estadoColumna = true;
							}
							for (int i = 0; i < 9; i++) {
								if (i == y) {
									for (int j = 0; j < 10; j++) {
										if (j == columnaUno || j == columnaDos) {
											if (!posicionesParedesHorizontales[i][j]) {
												posicionesParedesHorizontales[i][j] = true;
												break;
											} else {
												columnaUno = 0;
												columnaDos = 0;
												System.out.println("\n La pared ya existe");
												System.out.println(
														"\n Por favor, vuelve a ingresar las coordenadas de la pared horizontal");
												y = 0;
												estadoY = false;
												while (!(y > 0 && y < 9)) {
													if (estadoY) {
														System.out.println("\nCoordenada incorrecta...");
													}
													System.out.println("\n" + nombresJugadores[jugadorActual]
															+ ", �en qu� coordenada del eje Y quiere situar la pared?");
													y = Integer.parseInt(entradaEscaner.nextLine());
													estadoY = true;
												}
												System.out.println("\n" + nombresJugadores[jugadorActual]
														+ ", �en qu� columnas quiere situar la pared?");
												columnaUno = 0;
												columnaDos = 0;
												estadoColumna = false;
												while (columnaUno == columnaDos && (!(columnaUno > 0 && columnaUno < 10)
														|| !(columnaDos > 0 && columnaDos < 10))) {
													if (estadoColumna) {
														System.out.println("\nColumna incorrecta...");
													}
													if (!(columnaUno > 0 && columnaUno < 10)) {
														System.out.println("Ingrese el valor de la primera columna:");
														columnaUno = Integer.parseInt(entradaEscaner.nextLine());
													}
													if (!(columnaDos > 0 && columnaDos < 10)) {
														System.out.println("Ingrese el valor de la segunda columna:");
														columnaDos = Integer.parseInt(entradaEscaner.nextLine());
													}
													estadoColumna = true;
												}
											}
										}
									}
									break;
								}
							}
						} else if (orientacionPared.equalsIgnoreCase("b")) {
							x = 0;
							boolean estadoX = false;
							while (!(x > 0 && x < 9)) {
								if (estadoX) {
									System.out.println("\nCoordenada incorrecta...");
								}
								System.out.println("\n" + nombresJugadores[jugadorActual]
										+ ", �en qu� coordenada del eje X quiere situar la pared?");
								x = Integer.parseInt(entradaEscaner.nextLine());
								estadoX = true;
							}
							System.out.println(
									"\n" + nombresJugadores[jugadorActual] + ", �en qu� filas quiere situar la pared?");
							filaUno = 0;
							filaDos = 0;
							boolean estadoFila = false;
							while (filaUno == filaDos
									&& (!(filaUno > 0 && filaUno < 10) || !(filaDos > 0 && filaDos < 10))) {
								if (estadoFila) {
									System.out.println("\nFila incorrecta...");
								}
								if (!(filaUno > 0 && filaUno < 10)) {
									System.out.println("Ingrese el valor de la primera fila:");
									filaUno = Integer.parseInt(entradaEscaner.nextLine());
								}
								if (!(filaDos > 0 && filaDos < 10)) {
									System.out.println("Ingrese el valor de la segunda fila:");
									filaDos = Integer.parseInt(entradaEscaner.nextLine());
								}
								estadoFila = true;
							}

							for (int i = 0; i < 10; i++) {
								if (i == x) {
									for (int j = 0; j < 9; j++) {
										if (j == filaUno || j == filaDos) {
											if (!posicionesParedesHorizontales[j][i]) {
												posicionesParedesHorizontales[j][i] = true;
												break;
											} else {
												filaUno = 0;
												filaDos = 0;
												System.out.println("\n La pared ya existe");
												System.out.println(
														"\n Por favor, vuelve a ingresar las coordenadas de la pared vertical");
												x = 0;
												estadoX = false;
												while (!(x > 0 && x < 9)) {
													if (estadoX) {
														System.out.println("\nCoordenada incorrecta...");
													}
													System.out.println("\n" + nombresJugadores[jugadorActual]
															+ ", �en qu� coordenada del eje X quiere situar la pared?");
													x = Integer.parseInt(entradaEscaner.nextLine());
													estadoX = true;
												}
												System.out.println("\n" + nombresJugadores[jugadorActual]
														+ ", �en qu� filas quiere situar la pared?");
												filaUno = 0;
												filaDos = 0;
												estadoFila = false;
												while (filaUno == filaDos && (!(filaUno > 0 && filaUno < 10)
														|| !(filaDos > 0 && filaDos < 10))) {
													if (estadoFila) {
														System.out.println("\nFila incorrecta...");
													}
													if (!(filaUno > 0 && filaUno < 10)) {
														System.out.println("Ingrese el valor de la primera fila:");
														filaUno = Integer.parseInt(entradaEscaner.nextLine());
													}
													if (!(filaDos > 0 && filaDos < 10)) {
														System.out.println("Ingrese el valor de la segunda fila:");
														filaDos = Integer.parseInt(entradaEscaner.nextLine());
													}
													estadoFila = true;
												}
											}
										}
									}
									break;
								}
							}
						}
					}

					System.out.println("");
					int contadorFilasParaFichas = 0;
					System.out.println("     1   2   3   4   5   6   7   8   9     ");
					numeroFilas = 1;
					coordenadaY = 1;
					for (int i = 0; i < 19; i++) {
						if (i % 2 == 0) {
							techosYPisosConPared = "";
							for (int m = 0; m < 8; m++) {
								for (int n = 0; n < 9; n++) {
									if (posicionesParedesHorizontales[m][n]) {
										y = m + 1;
										if (jugadorActual == 0) {
											techosYPisosConPared = techosYPisosConPared + paredHorizontalParaFichaUno;
										} else if (jugadorActual == 1) {
											techosYPisosConPared = techosYPisosConPared + paredHorizontalParaFichaDos;
										} else {
											techosYPisosConPared = techosYPisosConPared + paredHorizontal;
										}
										posicionesParedesHorizontales[m][n] = false;
									}
								}
							}
							if (coordenadaY != 10) {
								System.out.println(techosYPisosConPared.replace("++", "+") + " " + coordenadaY + " ");
							} else {
								System.out.println(techosYPisosConPared.replace("++", "+") + " " + coordenadaY);
							}
							if ((y + 1) != coordenadaY) {
								if (coordenadaY != 10) {
									System.out.println(techosYPisos + " " + coordenadaY + " ");
								} else {
									System.out.println(techosYPisos + " " + coordenadaY);
								}
							}
							coordenadaY++;
							if ((i + 1) == 19) {
								System.out.println("   1   2   3   4   5   6   7   8   9   10  ");
							}
						} else {
							boolean estadoFilaSinFichas = true;
							boolean estadoFilaSinParedes = true;
							fila = "";

							for (int m = 0; m < 9; m++) {
								for (int n = 0; n < 8; n++) {
									if (posicionesParedesVerticales[m][n]) {
										x = n + 1;
										if (jugadorActual == 0) {
											fila = fila + paredVerticalParaFichaUno;
										} else if (jugadorActual == 1) {
											fila = fila + paredVerticalParaFichaDos;
										}
										posicionesParedesHorizontales[m][n] = false;
									} else {
										if (contadorFilasParaFichas == filaActualJugadorUno
												|| contadorFilasParaFichas == filaActualJugadorDos) {
											for (int j = 0; j < 9; j++) {
												if (contadorFilasParaFichas == filaActualJugadorUno
														&& j == columnaActualJugadorUno) {
													fila = fila + casillaConFichaUno;
												} else if (contadorFilasParaFichas == filaActualJugadorDos
														&& j == columnaActualJugadorDos) {
													fila = fila + casillaConFichaDos;
												} else {
													fila = fila + casillaSinFicha;
												}
											}
										}
									}
								}
							}
							for (int m = 0; m < 9; m++) {
								for (int n = 0; n < 8; n++) {
									if (posicionesParedesVerticales[m][n]) {
										x = n + 1;
										if (contadorFilasParaFichas == filaActualJugadorUno
												|| contadorFilasParaFichas == filaActualJugadorDos) {
											for (int j = 0; j < 9; j++) {
												if (contadorFilasParaFichas == filaActualJugadorUno
														&& j == columnaActualJugadorUno) {
													fila = fila + casillaConFichaUno;
												} else if (contadorFilasParaFichas == filaActualJugadorDos
														&& j == columnaActualJugadorDos) {
													fila = fila + casillaConFichaDos;
												} else {
													fila = fila + casillaSinFicha;
												}
											}
										}
									} else {
										if (n == 7) {
											estadoFilaSinParedes = true;
										}
										if (contadorFilasParaFichas == filaActualJugadorUno
												|| contadorFilasParaFichas == filaActualJugadorDos) {
											for (int j = 0; j < 9; j++) {
												if (contadorFilasParaFichas == filaActualJugadorUno
														&& j == columnaActualJugadorUno) {
													fila = fila + casillaConFichaUno;
												} else if (contadorFilasParaFichas == filaActualJugadorDos
														&& j == columnaActualJugadorDos) {
													fila = fila + casillaConFichaDos;
												} else {
													fila = fila + casillaSinFicha;
												}
											}
										}
									}
									posicionesParedesVerticales[m][n] = false;
								}
								System.out.println(" " + numeroFilas + " " + fila.replace("||", "|") + "   ");
								estadoFilaSinParedes = false;
								estadoFilaSinFichas = false;
							}

							if (estadoFilaSinFichas && estadoFilaSinParedes) {
								System.out.println(" " + numeroFilas + " " + filaSinFichasYSinParedes + "   ");
							} else {
								if (estadoFilaSinFichas && !estadoFilaSinParedes) {

								} else if (!estadoFilaSinFichas && estadoFilaSinParedes) {

								}
							}
							contadorFilasParaFichas++;
							numeroFilas++;
						}
					}

					if (filaActualJugadorUno == 8) {
						System.out.println("\nFelicidades " + nombresJugadores[0] + " has ganado el juego...");
						juegoTerminado = true;
					}
					if (filaActualJugadorDos == 0) {
						System.out.println("\nFelicidades " + nombresJugadores[1] + " has ganado el juego...");
						juegoTerminado = true;
					}

					if (jugadorActual == (cantidadJugadores - 1)) {
						jugadorActual = 0;
					} else {
						jugadorActual++;
					}
				}
			}
//			else if (perspectivaJuego.equalsIgnoreCase("b"))
//
//			{
//				System.out.println("     1   2   3   4   5   6   7   8   9     ");
//				int numeroFilas = 1;
//				int coordenadaY = 1;
//				for (int i = 0; i < 19; i++) {
//					if (i % 2 == 0) {
//						System.out.println(techosYPisos + " " + coordenadaY);
//						coordenadaY++;
//						if ((i + 1) == 19) {
//							System.out.println("   1   2   3   4   5   6   7   8   9   10");
//						}
//					} else {
//						if (i == 9) {
//							System.out.println(" " + numeroFilas + " | 1 |   |   |   |   |   |   |   | 2 |");
//						} else {
//							System.out.println(" " + numeroFilas + " " + filaSinFichasYSinParedes);
//						}
//						numeroFilas++;
//					}
//				}
//			}
		}
//		else if (cantidadJugadores == 4) {
//			System.out.println("\n");
//		}
	}

}